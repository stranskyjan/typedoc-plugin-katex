# [typedoc-plugin-katex](https://www.npmjs.com/package/typedoc-plugin-katex)
[![npm](https://img.shields.io/npm/v/typedoc-plugin-katex)](https://www.npmjs.com/package/typedoc-plugin-katex)

A plugin for [TypeDoc](https://typedoc.org/) to display mathematics with [KaTeX](https://katex.org/) using its [auto-render extension](https://katex.org/docs/autorender.html)

Strongly inspired by [this issue](https://github.com/TypeStrong/typedoc/issues/1799)

## Demo
Below is example documentation comment and screenshot of the result.
Here math is delimited by `$` and `$$` symbols, see [configuration and options](#configuration-and-options).

See also
[actual result](https://stranskyjan.gitlab.io/typedoc-plugin-katex/modules.html#normalDistribution)
and
[actual source code](https://gitlab.com/stranskyjan/typedoc-plugin-katex/-/tree/pages/example/example.ts#L12)
.

```ts
/**
 * Compute normal distribution
 * $$f(x)=\frac{1}{\sigma\sqrt{2\pi}}e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}$$
 *
 * @param x - argument of $f(x)$
 * @param mean - mean $\mu$
 * @param stdDev - standard deviation $\sigma$
 * @returns value $f(x)$
 */
export function normalDistribution(x: number, mean: number, stdDev: number) {
    ...
}
```
![demo](https://unpkg.com/typedoc-plugin-katex@0.1.2/assets/demo.png)

## Installation
```
npm install --save-dev typedoc-plugin-katex
```

## Usage
Plugin should be automatically detected by TypeDoc.
If not, see [TypeDoc `plugin` option](https://typedoc.org/guides/options/#plugin).

### KaTeX - Markdown interaction
TypeDoc
[uses markdown](https://typedoc.org/guides/doccomments/#markdown),
which is evaluated at compile time, i.e. before KaTeX.
Therefore, **equations might be modified and corrupted (!)**

See
[possible solutions](https://stranskyjan.gitlab.io/typedoc-plugin-katex/index.html#katex---markdown-interaction)
.

### Configuration and options
Extend your [TypeDoc config file](https://typedoc.org/guides/options/) with a new option named `katex`, e.g.:
```json
"katex": {
    "version": "0.11.1",
    "options": {
        "delimiters": [
            {"left": "$$", "right": "$$", "display": true},
            {"left": "$", "right": "$", "display": false}
        ],
        "macros": {
            "\\einstein": "E=mc^2"
        }
    }
}
```
or see `example/options.json` file

#### Items:

- `version` (`string`, optional)

    KaTeX version.

    If not specified, latest KaTeX version is used.

- `options` (`object`, optional)

    KaTeX options passed to `renderMathInElement`.
    See [this](https://katex.org/docs/autorender.html) and/or [this](https://katex.org/docs/options.html) KaTeX documentation for more info.
    
    Useful to specify math delimiters (e.g. `$` symbol) and user-defined KaTeX macros / commands.

## Compatibility
tested with [TypeDoc 0.22.15](https://www.npmjs.com/package/typedoc/v/0.22.15)

## Testing
- `npm run build`
- `npm run test`
- view `public/index.html`

## Contributing
is welcome :-)
- via the [GitLab pages of the project](https://gitlab.com/stranskyjan/typedoc-plugin-katex)

#### Bugs
[issue tracker](https://gitlab.com/stranskyjan/typedoc-plugin-katex/issues)

## Maintainer
[Jan Stránský](https://stranskyjan.cz)

## License
[MIT](https://opensource.org/licenses/MIT)

## TODO
- other elements than `document.body`?
- better API?
- "server side" rendering?
- ... ?
