/* Overview:
 * - use window.__typeDocPluginKatexOptions variable for the KaTeX options
 * - use https://cdn.jsdelivr.net/npm/katex/dist/... for not specified version
 * - use https://cdn.jsdelivr.net/npm/katex@x.y.z/dist/... if version is specified
 */

import { JSX, ParameterType } from "typedoc";
import type { Application } from "typedoc";

type KatexOptions = { [key: string]: any; }; // TODO?

declare module "typedoc" {
	export interface TypeDocOptionMap {
		replaceText: PluginConfig;
	}
}

type PluginConfig = {
	version?: string;
	options?: KatexOptions;
};

export function load(app: Application) {
	app.options.addDeclaration({
		name: "katex",
		help: "KaTeX plugin config",
		type: ParameterType.Mixed,
		defaultValue: {},
	});
	//
	app.renderer.hooks.on("head.end", () => {
		var { version, options } = app.options.getValue("katex") as PluginConfig;
		// katex url
		var katexUrl = "https://cdn.jsdelivr.net/npm/katex";
		if (version) katexUrl += "@" + version;
		katexUrl += "/dist/";
		// stringify options
		if (!options) optionsStr = `${undefined}`;
		else var optionsStr = JSON.stringify(options);
		// additional head elements
		return <>
			{/* katex stuff */}
			<link rel="stylesheet" href={katexUrl + "katex.min.css"} />
			<script src={katexUrl + "katex.min.js"} />
			<script src={katexUrl + "contrib/auto-render.min.js"} />
			{/* assign options to window.__typeDocPluginKatexOptions and katex auto-render using these options*/}
			<script>
				<JSX.Raw html={`
					window.__typeDocPluginKatexOptions=${optionsStr};
					window.addEventListener('load', () => window.renderMathInElement(document.body, window.__typeDocPluginKatexOptions));
				`} />
			</script>
		</>;
	});
};
