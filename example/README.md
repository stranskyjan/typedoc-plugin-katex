# Example documentation using [typedoc-plugin-katex](https://www.npmjs.com/package/typedoc-plugin-katex)

Use $\KaTeX$ for math in typedoc documentation!

E.g. formula from the [KaTeX main page](https://katex.org/):

<p>
$$
% \f is defined as #1f(#2) using the macro
\f\relax{x} = \int_{-\infty}^\infty
    \f\hat\xi\,e^{2 \pi i \xi x}
    \,d\xi
$$
</p>

Inline math: $\frac{a}{b}$

Display mode: $$\frac{a}{b}$$

User defined command `\einstein`: $\einstein$

## KaTeX - Markdown interaction
TypeDoc
[uses markdown](https://typedoc.org/guides/doccomments/#markdown),
which is evaluated at compile time, i.e. before KaTeX.
Therefore, **equations might be modified and corrupted (!)**

Typcially the problem is with:
- `\` followed by one non-letter character, like `\%`, `\{`, `\,` etc, where the backslash is omitted by markdown
- underscore `_`, which indicates _italics_ in markdown
- some combinations of characters creating a HTML syntax (e.g. `<b>`)

Consider these formulas and how they are displayed:
- percent `\%` is converted to plain `%`, LaTeX comment symbol:<br>
  `10 \% + 20 \%`<br>
  $10 \% + 20 \%$
- other backslash + non-letter symbol, treated as symbols without backslash:<br>
  `a\,b\{c\}\ d\|e\!f \begin{matrix}a \\ b\end{matrix}`<br>
  $a\,b\{c\}\ d\|e\!f \begin{matrix}a \\ b\end{matrix}$
- underscore `_` couple is converted to italics:<br>
  `a _bc_ d`<br>
  $a _bc_ d$
- combination of come symbols creates html:<br>
  `a<b>c, x</b>z`<br>
  $a<b>c, x</b>z$

### Solutions
Below are some of possible solutions, sorted by the author's preferences.

#### HTML blocks
Under [certain circumstances](https://github.github.com/gfm/#html-blocks),
text inside HTML is not processed by markdown.

Basically:
- all block has to be HTML, not just part
- morevover, the opening HTML tag has to be on "separate line". After block end, there should be "empty line"

##### Examples:
- ```
  math <span>$a\,b$</span> does not work, `markdown` *syntax* **works**.
  ```
  math <span>$a\,b$</span> does not work, `markdown` *syntax* **works**.

- ```
  <span>math $a\,b$ does not work, `markdown` *syntax* **works**.</span>
  ```
  <span>math $a\,b$ does not work, `markdown` *syntax* **works**.</span>

- ```
  <span>
  math $a\,b$ works, but `markdown` *syntax* **not**.</span>
  ```
  <span>
  math $a\,b$ works, but `markdown` *syntax* **not**.</span>

- ```
  <span>
  math $a\,b$ works, <code>html</code> <em>syntax</em> <strong>works</strong>.</span>
  ```
  <span>
  math $a\,b$ works, <code>html</code> <em>syntax</em> <strong>works</strong>.</span>

##### Rendering:
- `10 \% + 20 \%`<br>
  <p>
  $10 \% + 20 \%$
  </p>
- `a\,b\{c\}\ d\|e\!f \begin{matrix}a \\ b\end{matrix}`<br>
  <p>
  $a\,b\{c\}\ d\|e\!f \begin{matrix}a \\ b\end{matrix}$
  </p>
- `a _bc_ d`<br>
  <p>
  $a _bc_ d$
  </p>
- `a<b>c, x</b>z`<br>
  <p>
  $a<b>c, x</b>z$
  </p>
- still needs improvement not to introduce `<b>` and `</b>` as HTML...<br>
  `a < b > c, x < /b > z`<br>
  <p>
  $a < b > c, x < /b > z$
  </p>

#### Custom commands
The problematic symbols can be defined as custom macros.

Leads to "non-universal" LaTeX code.
- `10 \percent + 20 \percent`<br>
  $10 \percent + 20 \percent$
- `a\thinSpace b\lCurl c\rCurl \space d\parallel e\negSpace f \begin{matrix}a \newline b\end{matrix}`<br>
  $a\thinSpace b\lCurl c\rCurl \space d\parallel e\negSpace f \begin{matrix}a \newline b\end{matrix}$
- `a\underscore bc\underscore d`<br>
  $a\underscore bc\underscore d$
- `a\lt b\gt c, x\lt/b\gt z`<br>
  $a\lt b\gt c, x\lt/b\gt z$

#### Escaping
The problematic symbols can be escaped.

Leads to non copy-pastable LaTeX code.
- `10 \\% + 20 \\%`<br>
  $10 \\% + 20 \\%$
- `a\\,b\\{c\\}\\ d\\|e\\!f \begin{matrix}a \\\\ b\end{matrix}`<br>
  $a\\,b\\{c\\}\\ d\\|e\\!f \begin{matrix}a \\\\ b\end{matrix}$
- `a \_bc\_ d`<br>
  $a \_bc\_ d$
- `a\<b\>c, x\</b\>z`<br>
  $a\<b\>c, x\</b\>z$
