/**
 * A few testing formulas.
 *
 * $\frac{a}{b}$, $\einstein$
 *
 * $$\frac{a}{b}$$
 * $$\einstein$$
 *
 * @module
 */

/**
 * Compute normal distribution
 * $$f(x)=\frac{1}{\sigma\sqrt{2\pi}}e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}$$
 *
 * @param x - argument of $f(x)$
 * @param mean - mean $\mu$
 * @param stdDev - standard deviation $\sigma$
 * @returns value $f(x)$
 */
export function normalDistribution(x: number, mean: number, stdDev: number) {
	const numerator = Math.exp(-(1 / 2) * ((x - mean) / stdDev) ** 2);
	const denominator = stdDev * Math.sqrt(2 * Math.PI);
	return numerator / denominator;
}

/**
 * Testing `$a\,b$`, which should displayed `a` and `b` with thin space, but **not** with comma.
 *
 * Raw: $a\,b$
 *
 * Escaping: $a\\,b$
 *
 * Command: $a\thinSpace b$
 *
 * HTML, not block: <span>$a\,b$</span>. `Some` **markdown** _syntax_.
 *
 * <span>
 * HTML, block: $a\,b$. `Some` **markdown** _syntax_.
 * </span>
 *
 * <p>
 * HTML, block: $a\,b$. <code>Some</code> <strong>html</strong> <em>syntax</em>.
 * </p>
 *
 * @param a <p>Lorem $a\,\text{km}$ ipsum</p>
 * @param b <p>Dolor $b\,\text{km}$ sit amet</p>
 * @returns <p>Consectetur $a\,b$ adipisici elit</p>
 *
 * **asdfasdf**
 */
export function testMarkdown(a: number, b: number) {
	return a * b;
}
